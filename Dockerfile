FROM python:3.7-alpine

RUN adduser -S -D -h /app/ operations
WORKDIR /app/

COPY requirements.txt .
RUN pip3 install -r requirements.txt --no-cache-dir
RUN apk add curl --no-cache

COPY server.py .

USER operations

HEALTHCHECK --interval=10s --timeout=3s \
  CMD curl -f http://localhost:8000/hello/testme || exit 1

EXPOSE 8000

ENTRYPOINT ["python3", "server.py"]
